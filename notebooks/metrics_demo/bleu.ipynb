{
 "cells": [
  {
   "metadata": {},
   "cell_type": "markdown",
   "source": [
    "# BLEU\n",
    "\n",
    "[GitHub repository](https://github.com/huggingface/evaluate/tree/main/metrics/bleu) for more details.\n",
    "\n",
    "__BLEU__ metric measures the ratio of identical n-grams between the prediction and one or multiple references. This metric is most often used for measuring the quality of *machine-translated* texts.\n",
    "\n",
    "&NewLine;\n",
    "\n",
    "## Output\n",
    "\n",
    "The metric outputs a general *__bleu score__*, as well as *__precisions__* score which gives the ratio of identical n-grams (1-gram, 2-gram, 3-gram and 4-gram respectively); the *__precisions__* score is not used for the code quality measurement but is useful for easier understanding of the metric. The metric also gives scores for *__brevity_penalty__* and *__length_ration__* which are irrelevant for the code-quality measurement.\n",
    "\n",
    "&NewLine;\n",
    "\n",
    "## Usage example\n",
    "\n",
    "The following is a simple example of the metric behaviour. The __predictions__ variable holds two strings predicted by a machine and the __references__ variable holds one or multiple baselines per *prediction*."
   ],
   "id": "4c2c28f9fc1cf12"
  },
  {
   "metadata": {
    "ExecuteTime": {
     "end_time": "2024-05-21T09:28:43.391467Z",
     "start_time": "2024-05-21T09:28:41.024909Z"
    }
   },
   "cell_type": "code",
   "source": [
    "import evaluate\n",
    "\n",
    "predictions = [\"hello there general kenobi\", \"foo bar foobar\"]\n",
    "references = [\n",
    "    [\"hello there general kenobi\", \"hello there !\"],\n",
    "    [\"foo bar foobar\"]\n",
    "]\n",
    "bleu = evaluate.load(\"bleu\")\n",
    "metric_results = bleu.compute(predictions=predictions, references=references)\n",
    "print(metric_results)"
   ],
   "id": "e95e3a3940cb4005",
   "execution_count": 1,
   "outputs": []
  },
  {
   "metadata": {},
   "cell_type": "markdown",
   "source": [
    "Note that at least one of the references are identical to the prediction and, therefore, the metric gives a perfect *__bleu__* score, as well as perfect *__precisions__* scores for the 1-gram to 4-gram sets of words.\n",
    "\n",
    "However, if we remove a single word from the __prediction__ or the __reference__, the obtained score is *null*."
   ],
   "id": "a299aebe2f24a4c8"
  },
  {
   "metadata": {
    "ExecuteTime": {
     "end_time": "2024-05-21T09:35:08.369531Z",
     "start_time": "2024-05-21T09:35:07.018866Z"
    }
   },
   "cell_type": "code",
   "source": [
    "import evaluate\n",
    "\n",
    "# Removing the word 'there' from the predictions variable\n",
    "predictions = [\"hello general kenobi\", \"foo bar foobar\"]\n",
    "references = [\n",
    "    [\"hello there general kenobi\", \"hello there !\"],\n",
    "    [\"foo bar foobar\"]\n",
    "]\n",
    "bleu = evaluate.load(\"bleu\")\n",
    "metric_results = bleu.compute(predictions=predictions, references=references)\n",
    "print(metric_results)"
   ],
   "id": "efbc087a75a05207",
   "execution_count": 2,
   "outputs": []
  },
  {
   "metadata": {
    "ExecuteTime": {
     "end_time": "2024-05-27T12:23:17.658309Z",
     "start_time": "2024-05-27T12:23:16.158923Z"
    }
   },
   "cell_type": "code",
   "source": [
    "import os\n",
    "import json\n",
    "import evaluate as ev\n",
    "\n",
    "\n",
    "def custom_sort_key(s):\n",
    "    # A sorting key used to sort strings in a length-lexicographic order (length and alphabetical order)\n",
    "    return len(s), s\n",
    "\n",
    "\n",
    "def code_cleanup(script, remove_assert=False):\n",
    "    # Function that removes any unnecessary components of a given script (comments & tests), leaving only the code lines\n",
    "\n",
    "    # Removing the test component of HumanEval implementation following 'METADATA' information\n",
    "    if 'METADATA' in script:\n",
    "        script = script.split('METADATA', 1)[0]\n",
    "    elif 'def check(candidate)' in script:\n",
    "        script = script.split('def check(candidate)', 1)[0]\n",
    "\n",
    "    script_lines = script.splitlines()\n",
    "\n",
    "    multi_line_comment = False\n",
    "    comment_index = []\n",
    "    assert_index = []\n",
    "    empty_line_index = []\n",
    "\n",
    "    for index, line in enumerate(script_lines):\n",
    "\n",
    "        # Indexing any assert statement\n",
    "        if remove_assert and 'assert' in line and line[0] == 'a':\n",
    "            assert_index.append(index)\n",
    "            continue\n",
    "\n",
    "        if not multi_line_comment:\n",
    "            if '#' in line:\n",
    "                # Indexing single-line comments\n",
    "                if line.strip()[0] == '#':\n",
    "                    comment_index.append(index)\n",
    "                # Removing comment component of the line\n",
    "                else:\n",
    "                    cleaned_up_line = line.split('#', 1)[0]\n",
    "                    script_lines[index] = cleaned_up_line\n",
    "                continue\n",
    "\n",
    "            # Indexing the first line of multi-line comments\n",
    "            if '\"\"\"' in line or \"'''\" in line:\n",
    "                comment_index.append(index)\n",
    "                if line.count('\"\"\"') == 1 or line.count(\"'''\") == 1:\n",
    "                    multi_line_comment = True\n",
    "                continue\n",
    "\n",
    "        # Adding indexes for multi-line comments\n",
    "        if multi_line_comment and ('\"\"\"' not in line and \"'''\" not in line):\n",
    "            comment_index.append(index)\n",
    "            continue\n",
    "\n",
    "        # Indexing the last line of multi-line comments\n",
    "        if multi_line_comment and ('\"\"\"' in line or \"'''\" in line):\n",
    "            multi_line_comment = False\n",
    "            comment_index.append(index)\n",
    "            continue\n",
    "\n",
    "        # Indexing new lines and blank lines\n",
    "        if len(line) == 0 or line.isspace():\n",
    "            empty_line_index.append(index)\n",
    "            continue\n",
    "\n",
    "    # Merging indexes for comments, empty lines and assert statements\n",
    "    [comment_index.extend(indexes) for indexes in (empty_line_index, assert_index)]\n",
    "\n",
    "    # Removing all the unnecessary parts of code\n",
    "    for index in sorted(comment_index, reverse=True):\n",
    "        del script_lines[index]\n",
    "\n",
    "    # Concatenating the list of script lines\n",
    "    clean_script = '\\n'.join(script_lines)\n",
    "    return clean_script\n",
    "\n",
    "\n",
    "def bleu_metric(humaneval=False, check_successful=False, check_failed=False, second_script=False, different_task=False):\n",
    "    \"\"\"\n",
    "    Function that applies the \"BLEU\" metric to the AI generated code\n",
    "    :param humaneval: if True, the chosen baseline is the human-made implementation from \"HumanEval\" dataset\n",
    "    :param different_task: if True, the chosen references are the implementations for a different task (task 1)\n",
    "    :param check_successful: if True, the chosen references are implementations with successful tests\n",
    "    :param second_script: choose a different good implementation as the baseline\n",
    "    :param check_failed: if True, the chosen references are implementations with failed tests/exec errors\n",
    "    :return the dictionary with all the scores, the average score as well as the variance\n",
    "    \"\"\"\n",
    "    if check_successful and check_failed:\n",
    "        print('Only one active parameter allowed between \"check_successful\" & \"check_failed\"')\n",
    "        exit(1)\n",
    "\n",
    "    funct_test_path_prefix = '../../exp_results/functionality_tests'\n",
    "\n",
    "    if humaneval:\n",
    "        baseline_script_path = '../../humaneval/000_has_close_elements.py'\n",
    "    elif second_script:\n",
    "        baseline_script_path = '../../../ai_code/chatgpt_temp_0.8/HumanEval_0/42.py'\n",
    "    else:\n",
    "        baseline_script_path = '../../../ai_code/chatgpt_temp_0.8/HumanEval_0/16.py'\n",
    "    baseline_file_name = baseline_script_path.split('/')[-1]\n",
    "\n",
    "    if different_task:\n",
    "        data_folder_path = '../../../ai_code/chatgpt_temp_0.8/HumanEval_1'\n",
    "    else:\n",
    "        data_folder_path = '../../../ai_code/chatgpt_temp_0.8/HumanEval_0'\n",
    "\n",
    "    tested_task = data_folder_path.split('/')[-1]\n",
    "\n",
    "    model_name = 'chatgpt'\n",
    "    model_temp = 'temp_0.8'\n",
    "\n",
    "    metric_name = 'bleu'\n",
    "    metric = ev.load(metric_name)\n",
    "\n",
    "    metric_dict = {'overall_score': 0, 'average_variance': 0}\n",
    "    file_names = []\n",
    "    overall_score = 0\n",
    "\n",
    "    with open(baseline_script_path, 'r') as f:\n",
    "        baseline = code_cleanup(f.read())\n",
    "\n",
    "    for path, folder, files in os.walk(data_folder_path):\n",
    "        for file_name in sorted(files, key=custom_sort_key):\n",
    "\n",
    "            # Avoiding comparison of the baseline to an identical prediction (i.e., comparing the baseline to the baseline)\n",
    "            if file_name == baseline_file_name:\n",
    "                continue\n",
    "\n",
    "            else:                  \n",
    "                test_file_path = os.path.join(funct_test_path_prefix, model_name, model_temp, f'{tested_task}.json')\n",
    "                with open(test_file_path, 'r') as f:\n",
    "                    funct_dict = json.load(f)\n",
    "                    \n",
    "                # Filtering implementations with successful or failed tests\n",
    "                if check_successful:\n",
    "                    if not funct_dict[file_name]['successful']:\n",
    "                        continue\n",
    "                elif check_failed:\n",
    "                    if funct_dict[file_name]['successful']:\n",
    "                        continue\n",
    "\n",
    "                file_names.append(file_name)\n",
    "\n",
    "                current_script_path = os.path.join(path, file_name)\n",
    "                with open(current_script_path) as f:\n",
    "                    script = code_cleanup(f.read(), remove_assert=True)\n",
    "\n",
    "                results = metric.compute(predictions=[script], references=[baseline])\n",
    "\n",
    "                score = results['bleu']\n",
    "                metric_dict[file_name] = score\n",
    "                overall_score += score\n",
    "\n",
    "    nb_scripts = len(metric_dict.keys()) - 2\n",
    "\n",
    "    overall_score /= nb_scripts\n",
    "\n",
    "    metric_dict['overall_score'] = overall_score\n",
    "\n",
    "    variance = 0\n",
    "\n",
    "    for file in file_names:\n",
    "        variance += abs(overall_score - metric_dict[file])\n",
    "\n",
    "    metric_dict['average_variance'] = variance / nb_scripts\n",
    "\n",
    "    return metric_dict"
   ],
   "id": "initial_id",
   "execution_count": 2,
   "outputs": []
  },
  {
   "metadata": {},
   "cell_type": "markdown",
   "source": [
    "## Applying BLEU metric to code samples \n",
    "Here we take as a baseline the first *good* implementation from *__chatgpt_temp_0.8 task 0__* (script *16.py*) and we compare it to all the other programs from this model that pass the tests.\n",
    "\n",
    "__Task 0__: given *'numbers'* a list of __float__ and *'threshold'* a single __float__, determine if in the given list there are at least two numbers that are closer than the given *threshold*."
   ],
   "id": "af023e26c5aacbb1"
  },
  {
   "metadata": {
    "ExecuteTime": {
     "end_time": "2024-05-26T18:01:49.028671Z",
     "start_time": "2024-05-26T18:01:47.113397Z"
    }
   },
   "cell_type": "code",
   "source": [
    "bleu_dict = bleu_metric(check_successful=True)\n",
    "\n",
    "for key, value in list(bleu_dict.items())[:2]:\n",
    "    print(f'{key}: {value}')"
   ],
   "id": "155e3630101555f1",
   "execution_count": 4,
   "outputs": []
  },
  {
   "metadata": {},
   "cell_type": "markdown",
   "source": "Here we take as a baseline the __second__ successful implementation from *__chatgpt_temp_0.8__* (script *42.py*) and compare it to all the other *good* scripts. Considering that both implement the same task and successfully pass all the tests, the __semantic__ of the two baselines are identical and therefore should yield similar score. However, due to the *__BLEU__* metric being heavily influenced by the *textual differences* between the __reference__ and the __prediction__, the obtained score is considerably different (*0.7* for the script __16.py__ and *0.6* for the script __42.py__) and, therefore, the metric is not suitable for *__code_quality__* measurement.\n",
   "id": "a61573799d90dd6e"
  },
  {
   "metadata": {
    "ExecuteTime": {
     "end_time": "2024-05-26T18:02:34.690828Z",
     "start_time": "2024-05-26T18:02:32.961080Z"
    }
   },
   "cell_type": "code",
   "source": [
    "bleu_dict = bleu_metric(check_successful=True, second_script=True)\n",
    "\n",
    "for key, value in list(bleu_dict.items())[:2]:\n",
    "    print(f'{key}: {value}')"
   ],
   "id": "34628025b3aed6cd",
   "execution_count": 5,
   "outputs": []
  },
  {
   "metadata": {},
   "cell_type": "markdown",
   "source": [
    "## Baseline scripts\n",
    "\n",
    "- ### Script 14.py\n",
    "    \n",
    "\n",
    "    from typing import List\n",
    "    \n",
    "    def has_close_elements(numbers: List[float], threshold: float) -> bool:\n",
    "        numbers.sort()\n",
    "        \n",
    "        for i in range(1, len(numbers)):\n",
    "            if abs(numbers[i] - numbers[i-1]) < threshold:\n",
    "                return True\n",
    "        return False\n",
    "    \n",
    "- ### Script 42.py\n",
    "\n",
    "\n",
    "    from typing import List\n",
    "\n",
    "    def has_close_elements(numbers: List[float], threshold: float) -> bool:\n",
    "        for i, num1 in enumerate(numbers):\n",
    "            for j, num2 in enumerate(numbers):\n",
    "                if i != j and abs(num1-num2) < threshold:\n",
    "                    return True\n",
    "        return False\n",
    "\n",
    "    \n",
    "Note that the two implementations share many similarities (the same *import*, the same *function name* and *signature*), with the only major difference being the fact that one employs a *single* __for loop__ while the other uses a *double neste* __for loop__.\n",
    "\n",
    "&NewLine;\n",
    "\n",
    "\n",
    "While the observations from the experiment thus far show that the metric is not suited for __code quality__ measurement, we can still study its behaviour in different configurations (comparing the *good* baseline script with unsuccessful implementations or taking a human-made script as a baseline)\n",
    "\n",
    "&NewLine;\n",
    "&NewLine;\n",
    "\n",
    "## Comparing with unsuccessful implementations"
   ],
   "id": "6003f89863e18619"
  },
  {
   "metadata": {
    "ExecuteTime": {
     "end_time": "2024-05-26T18:03:09.195039Z",
     "start_time": "2024-05-26T18:03:07.100353Z"
    }
   },
   "cell_type": "code",
   "source": [
    "bleu_dict = bleu_metric(check_failed=True)\n",
    "\n",
    "for key, value in list(bleu_dict.items())[:2]:\n",
    "    print(f'{key}: {value}')"
   ],
   "id": "b815b9dbba212f9a",
   "execution_count": 8,
   "outputs": []
  },
  {
   "metadata": {},
   "cell_type": "markdown",
   "source": [
    "As we can clearly see, comparing a *good* implementation (script *16.py*) with *bad* implementations yields results that are similar to those obtained when comparing with *successful* implementations; this is due to the source of error from this particular case (*__chatgpt_temp_0.8 task 0__* ) where the failed tests are mostly caused by the word \"__python__\" appearing at the beginning of the script (probably the model forgets to comment the name of the programming language) or the lack of the \"import List\" that is needed in the script. Without these small errors, the majority of the generated implementations for this __model__ and __task__ would pass all the tests. \n",
    "\n",
    "Once again, due to the *BLEU* metric relying  heavily on the *textual similarities* and not paying any attention to such crucial elements like __missing imports__, __syntax errors__, etc., implementations that look similar to the *baseline* will yield good results, despite the code not being suitable for __compilation__ or __execution__.\n",
    "\n",
    "&NewLine;\n",
    "&NewLine;\n",
    "\n",
    "# Human-made baseline implementation\n",
    "\n",
    "The following is the human-made implementation for the *task 0* from Eval+ benchmark:\n",
    "\n",
    "    from typing import List\n",
    "    \n",
    "    def has_close_elements(numbers: List[float], threshold: float) -> bool:\n",
    "        assert threshold > 0, \"invalid inputs\"\n",
    "        assert all([isinstance(v, (int, float)) for v in numbers]), \"invalid inputs\"\n",
    "    \n",
    "        sorted_numbers = sorted(numbers)\n",
    "        for i in range(len(sorted_numbers) - 1):\n",
    "            if sorted_numbers[i + 1] - sorted_numbers[i] < threshold:\n",
    "                return True\n",
    "        return False\n",
    "\n",
    "Note that this implementation is very similar to the AI-generated ones, with the only notable difference being the presence of *assert* statements that check for the adequate __input__ values."
   ],
   "id": "32cb5f4332dc068a"
  },
  {
   "metadata": {
    "ExecuteTime": {
     "end_time": "2024-05-26T18:05:51.524330Z",
     "start_time": "2024-05-26T18:05:49.591535Z"
    }
   },
   "cell_type": "code",
   "source": [
    "bleu_dict = bleu_metric(check_successful=True, humaneval=True)\n",
    "\n",
    "for key, value in list(bleu_dict.items())[:2]:\n",
    "    print(f'{key}: {value}')"
   ],
   "id": "2008fc226dcb46f1",
   "execution_count": 9,
   "outputs": []
  },
  {
   "metadata": {},
   "cell_type": "markdown",
   "source": [
    "The addition of *assert* statements has a huge impact on the __BLEU__ result, despite the rest of the implementation being very similar to the AI-generated code.\n",
    "\n",
    "&NewLine;\n",
    "\n",
    "## Different task as a baseline\n",
    "\n",
    "Here we will once again take as the __baseline__ the first *good* script from __task 0__, but we will compare it to *correct* implementations of the __task 1__ of the Eval+ benchmark. This type of experiment is meant to test the capability of a given *metric* to detect differences in the code's __semantics__, a rather important functionality in the measurement of the *code quality*.\n",
    "\n",
    "&NewLine;\n",
    "\n",
    "__Task 1__: given *'paren_string'* a string with multiple groups of nested parentheses, return a list of strings with the separated groups of the nested parentheses.\n",
    "\n",
    "The following is an example of a *good* AI-generated implementation for __task 1__ (script *0.py*)\n",
    "\n",
    "    from typing import List\n",
    "    \n",
    "    def separate_paren_groups(paren_string: str) -> List[str]:\n",
    "        paren_string = paren_string.replace(\" \", \"\")\n",
    "        groups = []\n",
    "        stack = []\n",
    "        start = 0\n",
    "        for i in range(len(paren_string)):\n",
    "            if paren_string[i] == '(':\n",
    "                stack.append('(')\n",
    "            elif paren_string[i] == ')':\n",
    "                stack.pop()\n",
    "                if not stack:\n",
    "                    groups.append(paren_string[start:i+1])\n",
    "                    start = i+1\n",
    "        return groups"
   ],
   "id": "488744ec9c7430c4"
  },
  {
   "metadata": {
    "ExecuteTime": {
     "end_time": "2024-05-27T12:37:07.443562Z",
     "start_time": "2024-05-27T12:37:04.143398Z"
    }
   },
   "cell_type": "code",
   "source": [
    "bleu_dict = bleu_metric(check_successful=True, different_task=True)\n",
    "\n",
    "for key, value in list(bleu_dict.items())[:2]:\n",
    "    print(f'{key}: {value}')"
   ],
   "id": "eb9f9d292a362375",
   "execution_count": 9,
   "outputs": []
  },
  {
   "metadata": {},
   "cell_type": "markdown",
   "source": "As expected, the __BLEU__ score is very small due to the big *textual differences* between the implementations from these two tasks.\n",
   "id": "8e113d5cc608678e"
  },
  {
   "metadata": {},
   "cell_type": "code",
   "execution_count": null,
   "source": "",
   "id": "53e0fad1e9f5e38d",
   "outputs": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 2
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython2",
   "version": "2.7.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
