import copy
import os
import json
from enum import Enum
import pandas as pd
import numpy as np

import matplotlib.pyplot as plt
import seaborn as sns

from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn import metrics
from sklearn.metrics import classification_report


class Metric(Enum):
    bleu = 0
    codebleu = 1
    rouge = 2
    meteor = 3
    chrf = 4


def run_logistic_regression(dataset_path):
    """
    Function that runs logistic regression over the LLM-generated script results (i.e., establish if a correlation
    exists between metric score and pass/fail label)
    :return: a json file with scores for precision, recall, f1, accuracy
    """
    dataset_name = dataset_path.split('/')[-1]
    metric_res_folder_path = f'../exp_results/{dataset_name}/metrics_calc'
    logreg_folder_path = f'../exp_results/{dataset_name}/metrics_logreg/iterations'

    if not os.path.exists(logreg_folder_path):
        os.makedirs(logreg_folder_path)

    for item in sorted(os.listdir(metric_res_folder_path)):
        if '.csv' in item:
            logreg_dict = {'decision_boundary': 0.5}
            logreg_test_pred_dict = {}

            # Iterate over csv files with the metric score and the pass/fail label of LLM-generated scripts
            metric_name = item

            metric_path = os.path.join(metric_res_folder_path, metric_name)
            metric_df = pd.read_csv(metric_path)

            metric_name = metric_name.split('.')[0]
            logreg_file_name = f'{metric_name}_logreg_iterations.json'
            test_pred_file_name = f'{metric_name}_logreg_test_pred.json'
            logreg_file_path = os.path.join(logreg_folder_path, logreg_file_name)
            test_pred_file_path = os.path.join(logreg_folder_path, test_pred_file_name)

            print(f'Analyzing metric: {metric_name}')

            if 'codebleu' in metric_name:
                x = metric_df[['codebleu']]
            else:
                x = metric_df[['score']]
            y = metric_df['pass']

            # Run 100 iterations of logistic regression with different split of train/test datasets
            for i in range(100):
                print(f'Iteration {i+1}')

                x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.25)

                logreg = LogisticRegression(random_state=16)
                logreg.fit(x_train, y_train)
                y_pred = logreg.predict(x_test)

                # Saving the classification evaluation results (precision, recall, f1, accuracy)
                logreg_results = classification_report(y_test, y_pred, target_names=['fail', 'pass'], output_dict=True)
                logreg_dict[f'iter_{i+1}'] = logreg_results

                # Saving the ground truth labels (pass/fail) and the predict labels
                logreg_test_pred_dict[f'iter_{i+1}'] = {}
                logreg_test_pred_dict[f'iter_{i+1}']['y_test'] = y_test.tolist()
                logreg_test_pred_dict[f'iter_{i+1}']['y_pred'] = y_pred.tolist()

            with open(logreg_file_path, 'w') as f:
                json.dump(logreg_dict, f, indent=2)
            with open(test_pred_file_path, 'w') as f:
                json.dump(logreg_test_pred_dict, f)


def run_logreg_codebleu(dataset_path):
    """
    Function that runs logistic regression over the LLM-generated script results (i.e., establish if a correlation
    exists between metric score and pass/fail label) based on all the scores from CodeBLEU metric
    :return: a json file with scores for precision, recall, f1, accuracy
    """
    dataset_name = dataset_path.split('/')[-1]
    metric_res_file_path = f'../exp_results/{dataset_name}/metrics_calc/codebleu.csv'
    logreg_folder_path = f'../exp_results/{dataset_name}/metrics_logreg/codebleu'

    if not os.path.exists(logreg_folder_path):
        os.makedirs(logreg_folder_path)

    logreg_dict = {'decision_boundary': 0.5}
    logreg_test_pred_dict = {}

    metric_name = 'codebleu'

    metric_df = pd.read_csv(metric_res_file_path)

    logreg_file_name = f'{metric_name}_logreg_iterations.json'
    test_pred_file_name = f'{metric_name}_logreg_test_pred.json'
    logreg_file_path = os.path.join(logreg_folder_path, logreg_file_name)
    test_pred_file_path = os.path.join(logreg_folder_path, test_pred_file_name)

    print(f'Analyzing metric: {metric_name}')

    feature_cols = ['codebleu', 'ngram_match_score', 'weighted_ngram_match_score', 'syntax_match_score',
                    'dataflow_match_score']

    x = metric_df[feature_cols]
    y = metric_df['pass']

    # Run 100 iterations of logistic regression with different split of train/test datasets
    for i in range(100):
        print(f'Iteration {i + 1}')

        x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.25)

        logreg = LogisticRegression(random_state=16)
        logreg.fit(x_train, y_train)
        y_pred = logreg.predict(x_test)

        # Saving the classification evaluation results (precision, recall, f1, accuracy)
        logreg_results = classification_report(y_test, y_pred, target_names=['pass', 'fail'], output_dict=True)
        logreg_dict[f'iter_{i+1}'] = logreg_results

        # Saving the ground truth labels (pass/fail) and the predict labels
        logreg_test_pred_dict[f'iter_{i + 1}'] = {}
        logreg_test_pred_dict[f'iter_{i+1}']['y_test'] = y_test.tolist()
        logreg_test_pred_dict[f'iter_{i+1}']['y_pred'] = y_pred.tolist()

    with open(logreg_file_path, 'w') as f:
        json.dump(logreg_dict, f, indent=2)
    with open(test_pred_file_path, 'w') as f:
        json.dump(logreg_test_pred_dict, f)


def run_logreg_codebleu_v2(dataset_path):
    """
    Function that runs logistic regression over the LLM-generated script results (i.e., establish if a correlation
    exists between metric score and pass/fail label) based on all the scores from CodeBLEU metric
    :return: a json file with scores for precision, recall, f1, accuracy
    """
    dataset_name = dataset_path.split('/')[-1]
    metric_res_file_path = f'../exp_results/{dataset_name}/metrics_calc/codebleu.csv'
    logreg_folder_path = f'../exp_results/{dataset_name}/metrics_logreg/codebleu'

    if not os.path.exists(logreg_folder_path):
        os.makedirs(logreg_folder_path)

    logreg_dict = {'decision_boundary': 0.5}
    logreg_test_pred_dict = {}

    metric_name = 'codebleu'

    metric_df = pd.read_csv(metric_res_file_path)

    logreg_file_name = f'{metric_name}_logreg_iterations_v2.json'
    test_pred_file_name = f'{metric_name}_logreg_test_pred_v2.json'
    logreg_file_path = os.path.join(logreg_folder_path, logreg_file_name)
    test_pred_file_path = os.path.join(logreg_folder_path, test_pred_file_name)

    print(f'Analyzing metric: {metric_name}')

    feature_cols = ['ngram_match_score', 'weighted_ngram_match_score', 'syntax_match_score',
                    'dataflow_match_score']

    x = metric_df[feature_cols]
    y = metric_df['pass']

    # Run 100 iterations of logistic regression with different split of train/test datasets
    for i in range(100):
        print(f'Iteration {i + 1}')

        x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.25)

        logreg = LogisticRegression(random_state=16)
        logreg.fit(x_train, y_train)
        y_pred = logreg.predict(x_test)

        # Saving the classification evaluation results (precision, recall, f1, accuracy)
        logreg_results = classification_report(y_test, y_pred, target_names=['pass', 'fail'], output_dict=True)
        logreg_dict[f'iter_{i+1}'] = logreg_results

        # Saving the ground truth labels (pass/fail) and the predict labels
        logreg_test_pred_dict[f'iter_{i + 1}'] = {}
        logreg_test_pred_dict[f'iter_{i+1}']['y_test'] = y_test.tolist()
        logreg_test_pred_dict[f'iter_{i+1}']['y_pred'] = y_pred.tolist()

    with open(logreg_file_path, 'w') as f:
        json.dump(logreg_dict, f, indent=2)
    with open(test_pred_file_path, 'w') as f:
        json.dump(logreg_test_pred_dict, f)


def divide_by_100(input_dict):
    # Function that divides the obtained scores for the average calculation
    input_dict['pass']['precision'] /= 100
    input_dict['pass']['recall'] /= 100
    input_dict['pass']['f1-score'] /= 100

    input_dict['fail']['precision'] /= 100
    input_dict['fail']['recall'] /= 100
    input_dict['fail']['f1-score'] /= 100

    input_dict['accuracy'] /= 100

    input_dict['macro avg']['precision'] /= 100
    input_dict['macro avg']['recall'] /= 100
    input_dict['macro avg']['f1-score'] /= 100

    input_dict['weighted avg']['precision'] /= 100
    input_dict['weighted avg']['recall'] /= 100
    input_dict['weighted avg']['f1-score'] /= 100


def logreg_average_variance(dataset_path, codebleu=False):
    # Function that measures the average and variance values of the 100 logreg-iteration results
    dataset_name = dataset_path.split('/')[-1]
    if codebleu:
        logreg_folder_path = f'../exp_results/{dataset_name}/metrics_logreg/codebleu'
    else:
        logreg_folder_path = f'../exp_results/{dataset_name}/metrics_logreg/iterations'

    metrics_template = {'precision': 0.0, 'recall': 0.0, 'f1-score': 0.0, 'support': 0}
    logreg_template = {'pass': copy.deepcopy(metrics_template),
                       'fail': copy.deepcopy(metrics_template),
                       'accuracy': 0.0,
                       'macro avg': copy.deepcopy(metrics_template),
                       'weighted avg': copy.deepcopy(metrics_template)}

    for file in sorted(os.listdir(logreg_folder_path)):
        if 'iterations' in file:
            current_file_path = os.path.join(logreg_folder_path, file)
            with open(current_file_path, 'r') as f:
                logreg_dict = json.load(f)

            metric_name = file.split('_')[0]
            if 'v2' in file:
                logreg_file_name = f'{metric_name}_logreg_avg-var_v2.json'
            else:
                logreg_file_name = f'{metric_name}_logreg_avg-var.json'

            avg_var_folder_path = logreg_folder_path.rpartition('/')[0]
            if codebleu:
                logreg_file_path = os.path.join(avg_var_folder_path, 'codebleu', logreg_file_name)
            else:
                logreg_file_path = os.path.join(avg_var_folder_path, logreg_file_name)

            logreg_avg_var = {
                "decision_boundary": 0.5,
                'average': copy.deepcopy(logreg_template),
                'variance': copy.deepcopy(logreg_template)}

            for iteration in list(logreg_dict.keys())[1:]:
                logreg_avg_var['average']['pass']['precision'] += logreg_dict[iteration]['pass']['precision']
                logreg_avg_var['average']['pass']['recall'] += logreg_dict[iteration]['pass']['recall']
                logreg_avg_var['average']['pass']['f1-score'] += logreg_dict[iteration]['pass']['f1-score']
                logreg_avg_var['average']['pass']['support'] += logreg_dict[iteration]['pass']['support']

                logreg_avg_var['average']['fail']['precision'] += logreg_dict[iteration]['fail']['precision']
                logreg_avg_var['average']['fail']['recall'] += logreg_dict[iteration]['fail']['recall']
                logreg_avg_var['average']['fail']['f1-score'] += logreg_dict[iteration]['fail']['f1-score']
                logreg_avg_var['average']['fail']['support'] += logreg_dict[iteration]['fail']['support']

                logreg_avg_var['average']['accuracy'] += logreg_dict[iteration]['accuracy']

                logreg_avg_var['average']['macro avg']['precision'] += logreg_dict[iteration]['macro avg']['precision']
                logreg_avg_var['average']['macro avg']['recall'] += logreg_dict[iteration]['macro avg']['recall']
                logreg_avg_var['average']['macro avg']['f1-score'] += logreg_dict[iteration]['macro avg']['f1-score']
                logreg_avg_var['average']['macro avg']['support'] += logreg_dict[iteration]['macro avg']['support']

                logreg_avg_var['average']['weighted avg']['precision'] += (
                    logreg_dict)[iteration]['weighted avg']['precision']
                logreg_avg_var['average']['weighted avg']['recall'] += (
                    logreg_dict)[iteration]['weighted avg']['recall']
                logreg_avg_var['average']['weighted avg']['f1-score'] += (
                    logreg_dict)[iteration]['weighted avg']['f1-score']
                logreg_avg_var['average']['weighted avg']['support'] += (
                    logreg_dict)[iteration]['weighted avg']['support']

            divide_by_100(logreg_avg_var['average'])

            for iteration in list(logreg_dict.keys())[1:]:
                logreg_avg_var['variance']['pass']['precision'] += abs(logreg_avg_var['average']['pass']['precision'] -
                                                                       logreg_dict[iteration]['pass']['precision'])
                logreg_avg_var['variance']['pass']['recall'] += abs(logreg_avg_var['average']['pass']['recall'] -
                                                                    logreg_dict[iteration]['pass']['recall'])
                logreg_avg_var['variance']['pass']['f1-score'] += abs(logreg_avg_var['average']['pass']['f1-score'] -
                                                                      logreg_dict[iteration]['pass']['f1-score'])
                logreg_avg_var['variance']['pass']['support'] = logreg_avg_var['average']['pass']['support']

                logreg_avg_var['variance']['fail']['precision'] += abs(
                    logreg_avg_var['average']['fail']['precision'] -
                    logreg_dict[iteration]['fail']['precision'])
                logreg_avg_var['variance']['fail']['recall'] += abs(
                    logreg_avg_var['average']['fail']['recall'] -
                    logreg_dict[iteration]['fail']['recall'])
                logreg_avg_var['variance']['fail']['f1-score'] += abs(
                    logreg_avg_var['average']['fail']['f1-score'] -
                    logreg_dict[iteration]['fail']['f1-score'])
                logreg_avg_var['variance']['fail']['support'] = logreg_avg_var['average']['fail']['support']

                logreg_avg_var['variance']['accuracy'] += abs(logreg_avg_var ['average']['accuracy'] -
                                                              logreg_dict[iteration]['accuracy'])

                logreg_avg_var['variance']['macro avg']['precision'] += abs(
                    logreg_avg_var['average']['macro avg']['precision'] -
                    logreg_dict[iteration]['macro avg']['precision'])
                logreg_avg_var['variance']['macro avg']['recall'] += abs(
                    logreg_avg_var['average']['macro avg']['recall'] -
                    logreg_dict[iteration]['macro avg']['recall'])
                logreg_avg_var['variance']['macro avg']['f1-score'] += abs(
                    logreg_avg_var['average']['macro avg']['f1-score'] -
                    logreg_dict[iteration]['macro avg']['f1-score'])
                logreg_avg_var['variance']['macro avg']['support'] = logreg_avg_var['average']['macro avg']['support']

                logreg_avg_var['variance']['weighted avg']['precision'] += abs(
                    logreg_avg_var['average']['weighted avg']['precision'] -
                    logreg_dict[iteration]['weighted avg']['precision'])
                logreg_avg_var['variance']['weighted avg']['recall'] += abs(
                    logreg_avg_var['average']['weighted avg']['recall'] -
                    logreg_dict[iteration]['weighted avg']['recall'])
                logreg_avg_var['variance']['weighted avg']['f1-score'] += abs(
                    logreg_avg_var['average']['weighted avg']['f1-score'] -
                    logreg_dict[iteration]['weighted avg']['f1-score'])
                logreg_avg_var['variance']['weighted avg']['support'] = (
                    logreg_avg_var)['average']['weighted avg']['support']

            divide_by_100(logreg_avg_var['variance'])

            with open(logreg_file_path, 'w') as f:
                json.dump(logreg_avg_var, f, indent=2)


def metric_name_to_title(metric_name):
    # Function that returns the name of a metric used in the confusion matrix representation
    title = ''
    match metric_name:
        case 'bleu':
            title = 'BLEU'
        case 'codebleu':
            title = 'CodeBLEU'
        case 'rouge':
            title = 'ROUGE'
        case 'meteor':
            title = 'METEOR'
        case 'chrf':
            title = 'ChrF'
    return title


def format_logreg_results(logreg_dict):
    row_format = '{:<12} {:>10.2f} {:>10.2f} {:>10.2f} {:>10}'
    row_format_accuracy = '{:<33}  {:>10.2f} {:>10}'

    formated_rows = []

    for label in ['pass', 'fail']:
        row = row_format.format(
            label,
            logreg_dict[label]['precision'],
            logreg_dict[label]['recall'],
            logreg_dict[label]['f1-score'],
            int(logreg_dict[label]['support'])
        )
        formated_rows.append(row)

    accuracy_row = row_format_accuracy.format(
        'accuracy',
        logreg_dict['accuracy'],
        int(logreg_dict['pass']['support'] + logreg_dict['fail']['support'])
    )

    for avg_label in ['macro avg', 'weighted avg']:
        avg_row = row_format.format(
            avg_label,
            logreg_dict[avg_label]['precision'],
            logreg_dict[avg_label]['recall'],
            logreg_dict[avg_label]['f1-score'],
            int(logreg_dict[avg_label]['support'])
        )
        formated_rows.append(avg_row)
    formated_rows.insert(2, f'\n{accuracy_row}')
    return '\n'.join(formated_rows)


def display_logreg_iterations(dataset_path, metric, num_iterations=5):
    dataset_name = dataset_path.split('/')[-1]
    metric_name = Metric(metric).name
    metric_title = metric_name_to_title(metric_name)
    logreg_file_path = f'../exp_results/{dataset_name}/metrics_logreg/iterations/{metric_name}_logreg_iterations.json'

    with open(logreg_file_path, 'r') as f:
        logreg_dict = json.load(f)

    print(f'\nLogistic Regression results for \"{metric_title}\" metric (first {num_iterations} iteration(s)):\n')
    for key in list(logreg_dict.keys())[1:num_iterations+1]:
        print(f'{key}:')
        print(f"{' ':<12} {'precision':>10} {'recall':>10} {'f1-score':>10} {'support':>10}")
        print(format_logreg_results(logreg_dict[key]))
        print('\n' + '-'*60 + '\n')


def display_logreg_avg_var(dataset_path, metric):
    dataset_name = dataset_path.split('/')[-1]
    metric_name = Metric(metric).name
    metric_title = metric_name_to_title(metric_name)
    logreg_file_path = f'../exp_results/{dataset_name}/metrics_logreg/{metric_name}_logreg_avg-var.json'

    with open(logreg_file_path, 'r') as f:
        logreg_dict = json.load(f)

    print(f'\nLogistic Regression results for \"{metric_title}\" metric (average and variance):\n')
    for key in list(logreg_dict.keys())[1:]:
        print(f'{key}:')
        print(f"{' ':<12} {'precision':>10} {'recall':>10} {'f1-score':>10} {'support':>10}")
        print(format_logreg_results(logreg_dict[key]))
        print('\n' + '-' * 60 + '\n')
    print('\n')


def generate_confusion_matrix(dataset_path, codebleu=False):
    # Generate the confusion matrix based on the ground truth and predicted labels of pass/fail
    dataset_name = dataset_path.split('/')[-1]

    if codebleu:
        log_reg_res_folder_path = f'../exp_results/{dataset_name}/metrics_logreg/codebleu'
        matrix_folder_path = f'../exp_results/{dataset_name}/metrics_logreg/confusion_matrix/codebleu'
    else:
        log_reg_res_folder_path = f'../exp_results/{dataset_name}/metrics_logreg/iterations'
        matrix_folder_path = f'../exp_results/{dataset_name}/metrics_logreg/confusion_matrix'

    if not os.path.exists(matrix_folder_path):
        os.makedirs(matrix_folder_path)

    # Generate the confusion matrix per code-quality metric
    for file in sorted(os.listdir(log_reg_res_folder_path)):
        if 'test_pred' in file:
            metric_name = file.split('_')[0]
            matrix_title = metric_name_to_title(metric_name)

            print(f'Analyzing metric: {metric_name}')

            if 'v2' in file:
                file_name = f'{metric_name}_v2.png'
            else:
                file_name = f'{metric_name}.png'

            image_file_path = os.path.join(matrix_folder_path, file_name)

            test_pred_file_path = os.path.join(log_reg_res_folder_path, file)
            with open(test_pred_file_path, 'r') as f:
                test_pred_dict = json.load(f)

            # Cumulate the 100 iteration confusion matrices
            cumulative_confusion_matrix = np.zeros((2, 2), dtype=int)

            for iteration in range(100):
                current_iteration = f'iter_{iteration+1}'
                y_test = test_pred_dict[current_iteration]['y_test']
                y_pred = test_pred_dict[current_iteration]['y_pred']

                current_confusion_matrix = metrics.confusion_matrix(y_test, y_pred)
                cumulative_confusion_matrix += current_confusion_matrix

            average_confusion_matrix = cumulative_confusion_matrix / 100

            class_names = ['fail', 'pass']
            fig, ax = plt.subplots(figsize=(8, 6))
            tick_marks = np.arange(len(class_names))
            plt.xticks(tick_marks, class_names)
            plt.yticks(tick_marks, class_names)

            # Create the heatmap
            sns.heatmap(pd.DataFrame(average_confusion_matrix), annot=True, cmap='YlGnBu', fmt='g',
                        xticklabels=class_names, yticklabels=class_names, ax=ax)
            ax.xaxis.set_label_position('top')
            plt.title(f'{matrix_title}', y=1.05)  # Adjust the title position
            plt.ylabel('Actual label')
            plt.xlabel('Predicted label')

            plt.tight_layout()  # Adjust layout to prevent clipping

            # Save and display the figure
            fig.savefig(image_file_path, dpi=96)
            plt.close(fig)


generate_confusion_matrix('../../ai_code')
