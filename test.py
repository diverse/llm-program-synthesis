import os
import json

nb_original = 0
nb_distinct = 0


for path, folders, files in os.walk('../ai_code'):
    for file in files:
        if file.endswith('.py'):
            nb_original += 1

for path, folders, files in os.walk('../ai_code_distinct'):
    for file in files:
        if file.endswith('.py'):
            nb_distinct += 1


print(nb_original)
print(nb_distinct)


def successful_test_counter(dataset_path):
    # Function that measures the rate of successful tests of the AI-generated code
    dataset_name = dataset_path.split('/')[-1]
    funct_test_folder_path = f'exp_results/{dataset_name}/functionality_tests'

    total_tests_counter = 0
    failed_tests_counter = 0

    for path, folders, files in os.walk(funct_test_folder_path):
        for file_name in files:
            test_file_path = os.path.join(path, file_name)
            with open(test_file_path, 'r') as f:
                model_dict = json.load(f)

            keys = model_dict.keys()
            total_tests_counter += len(keys)

            for key in list(model_dict.keys())[1:]:
                if not model_dict[key]['successful']:
                    failed_tests_counter += 1

    print(f'Total number of tests:  {total_tests_counter}')
    print(f'Number of failed tests: {failed_tests_counter}')

    rate_failed_tests = (100 / total_tests_counter) * failed_tests_counter
    print(f'Rate of failed tests: {rate_failed_tests}%')


# successful_test_counter('../../ai_code')
successful_test_counter('../ai_code_distinct')
