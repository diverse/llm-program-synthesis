import json
import requests
import os
from datetime import datetime

"""
Abbreviations for key terms:
    rg - response generation
"""

api_url = 'http://localhost:11434/api/generate'

model = "codellama:7b"

system = "You're an AI specialized in Python code generation. Give only the implementation; don't test the code."

prompt = "Write a Python program that prints out the Fibonacci sequence."

# Dictionary of the parameters used for the rg
params = {
    "model": model,
    "system": system,
    "prompt": prompt,
    "stream": False,
    "options": {
        "temperature": 0.0
    }
}

initial_time = datetime.now()
# Post request to the ollama API
response = requests.post(api_url, json=params)

exec_time = datetime.now()

diff = exec_time - initial_time

duration_in_s = diff.total_seconds()

print(f"Exec duration in seconds: {duration_in_s}\n")

if response.status_code == 200:
    # Decode the JSON response
    extracted_response = response.json()

    # Create a folder named after the model if it doesn't exist
    path_prefix = "test"  # Folder name for all the generated JSON files
    model_folder = model.replace(':', '_')  # Replace ':' with '_' for the folder name
    if not os.path.exists(f"{path_prefix}/{model_folder}"):
        os.makedirs(f"{path_prefix}/{model_folder}")

    # Generate filename with the current date and time
    current_datetime = datetime.now().strftime("%d-%m-%Y_%H-%M-%S")
    filename_json = f"{current_datetime}.json"
    filename_text = f"{current_datetime}.txt"

    # Save the generated JSON response to a file
    filepath = os.path.join(path_prefix, model_folder, filename_json)
    with open(filepath, 'w') as file:
        json.dump(extracted_response, file, indent=4)
        # print(f"Saved full response to '{filepath}'")

    filepath = os.path.join(path_prefix, model_folder, filename_text)
    with open(filepath, 'w') as file:
        extracted_code = extracted_response['response']
        file.write(extracted_code)
        # print(f"Saved extracted code to '{filepath}'")

    print(extracted_code)

else:
    print('Error: ', response.status_code)


"""
if response.status_code == 200:
    # Decode the JSON response
    decoded_response = response.json()

    extracted_code = decoded_response['response']

    extracted_code = extracted_code.replace('```', '')

    print(repr(extracted_code))
    print(extracted_code + '\n')

    tree = ast.parse(extracted_code)
    pretty_print = ast.unparse(extracted_code)

    print(repr(extracted_code) + '\n')
"""

"""
with open("tmp.txt", 'r') as f:
    f.write(code)
"""